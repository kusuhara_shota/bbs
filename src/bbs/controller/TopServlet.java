package bbs.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Message;
import bbs.beans.UserComment;
import bbs.beans.UserMessage;
import bbs.service.CommentService;
import bbs.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String category = request.getParameter("category");
		String oldDate = request.getParameter("oldDate");
		String newDate = request.getParameter("newDate");



		if (StringUtils.isEmpty(oldDate)) {

			oldDate = new MessageService().getOldDate().getCreated_at().toString();
		}
		if (StringUtils.isEmpty(newDate)) {
;
			newDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}


		List<UserMessage> messages = new MessageService().getMessage
				(category, oldDate + " 00:00:00", newDate + " 23:59:59");
		List<UserComment> comments = new CommentService().getComment();
		List<Message> categories = new MessageService().getCategories();


		request.setAttribute("messages",  messages);
		request.setAttribute("comments",  comments);
		request.setAttribute("categories", categories);
		request.setAttribute("category", category);
		System.out.println(oldDate);
		request.setAttribute("oldDate", oldDate);
		System.out.println(newDate);
		request.setAttribute("newDate", newDate);

		request.getRequestDispatcher("top.jsp").forward(request,response);

		HttpSession session = request.getSession();
		session.removeAttribute("errorMessages");
	}

}
