package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Department;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.DepartmentService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/userSetting" })
@MultipartConfig(maxFileSize = 100000)
public class UserSettingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<Branch> branches = new BranchService().getBranch();
		request.setAttribute("branches",  branches);
		List<Department> departments = new DepartmentService().getDepartment();
		request.setAttribute("departments", departments);

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		try {
			Integer.parseInt(request.getParameter("id"));
		} catch (NumberFormatException e){
			messages.add("不正なアクセスです");
			System.out.println("不正なアクセスです");


			List<User> users = new UserService().getUsers();
			request.setAttribute("users",  users);

			session.setAttribute("errorMessages", messages);
			response.sendRedirect("userManagement");

			return;
		}

		int id = (Integer.parseInt(request.getParameter("id")));

			User editUser = new UserService().getUser(id);

			if (editUser == null) {
				messages.add("指定のアカウントは存在しません");
				session.setAttribute("errorMessages", messages);

				request.setAttribute("editUser", editUser);
				response.sendRedirect("userManagement");

				return;
			}

		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("userSetting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		List<String> messages = new ArrayList<String>();


		if (isValid(request, messages) == true) {

			User editUser = getEditUser(request, messages);

			request.setAttribute("editUser", editUser);

			new UserService().update(editUser);

			response.sendRedirect("userManagement");

		} else {
			List<Branch> branches = new BranchService().getBranch();
			request.setAttribute("branches",  branches);
			List<Department> departments = new DepartmentService().getDepartment();
			request.setAttribute("departments", departments);

			User editUser = getEditUser(request, messages);

			request.setAttribute("editUser", editUser);

			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("userSetting.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request, List<String> messages)
			throws IOException, ServletException {

		int id = (Integer.parseInt(request.getParameter("user_id")));
		System.out.println(id);
		User editUser = new UserService().getUser(id);

		editUser.setLogin_id(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		try {
			editUser.setId(Integer.parseInt(request.getParameter("user_id")));
		} catch (NumberFormatException e) {
			editUser.setBranch_id(0);
		}
		try {
			editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
		} catch (NumberFormatException e) {
			editUser.setBranch_id(0);
		}
		try {
			editUser.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
		} catch (NumberFormatException e) {
			editUser.setDepartment_id(0);
		}
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String confirm_password = request.getParameter("confirm_password");
		String name = request.getParameter("name");
		String branch_id = request.getParameter("branch_id");
		String department_id = request.getParameter("department_id");

		System.out.println(branch_id);

		int id = (Integer.parseInt(request.getParameter("user_id")));
		User existingUser = new UserService().getUser(login_id);

		if (StringUtils.isEmpty(login_id) == true) {
			messages.add("ログインIDを入力してください");
		} else {
			if (!(login_id.matches("^[0-9a-zA-Z]{6,20}$"))) {
				messages.add("ログインIDは6～20文字の半英数字で入力してください");
			} else {
				if (!(existingUser == null)) {
					if (id != existingUser.getId()) {
						messages.add("既存のログインIDです");
					}
				}
			}
		}
		if ((password.equals(confirm_password)) == false) {
			messages.add("パスワードと確認用パスワードが一致しません");
		} else {
			if (!(StringUtils.isEmpty(password))) {
				if (!(password.matches("^[!-~]{6,25}$"))) {
					messages.add("パスワードは6～25文字の半角文字(記号可)で入力してください");
				}
			}
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else {
			if (!(name.length() <= 10)) {
				messages.add("名前は10文字以下で入力してください");
			}
		}
		if (StringUtils.isEmpty(branch_id) == true) {
			messages.add("支店を選択してください");

		}
		if (StringUtils.isEmpty(department_id) == true) {
			messages.add("役職を選択してください");
		}

		try {
			Integer.parseInt(request.getParameter("branch_id"));
		} catch (NumberFormatException e){
		}
		try {
			Integer.parseInt(request.getParameter("department_id"));
		} catch (NumberFormatException e){
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}