package bbs.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;

@WebFilter("/*")
public class AuthorityFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		StringBuffer requestUrl = ((HttpServletRequest) request).getRequestURL();
		String requestUrlStr = requestUrl.toString();


		HttpSession session = ((HttpServletRequest)request).getSession();

		User user = (User)session.getAttribute("loginUser");
		System.out.println(user);

		if (user != null) {
			if ((requestUrlStr.endsWith("/userManagement"))
					|| (requestUrlStr.endsWith("/userSetting"))
						|| (requestUrlStr.endsWith("/signup"))) {

				if (!(((user.getBranch_id()) == 1) && ((user.getDepartment_id()) == 1))) {

					List<String> messages = new ArrayList<String>();
					messages.add("権限がありません");
					session.setAttribute("errorMessages", messages);

					((HttpServletResponse)response).sendRedirect("./");

					return;
				}
			}
		}
		chain.doFilter(request, response);

	}


	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}